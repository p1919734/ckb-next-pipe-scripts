#!/usr/bin/env

# Thoses variable may be tweaked to the user's preference
timer="0.8"
ckb_pipe="/tmp/ckbpipe000"
ckb_git_status_file="/tmp/ckb-next_git-status_pid"

# reset the color
echo "rgb 00000000" >$ckb_pipe

if [ -f $ckb_git_status_file ] ; then
	echo -e "\n$0 was already launched, closing all instances."
	kill $(cat "$ckb_git_status_file") >/dev/null 2>&1
	rm "$ckb_git_status_file" >/dev/null 2>&1
else
	echo $$ > $ckb_git_status_file
	while true ; do
		customPPID=$(ps -o ppid= -p $$)
		cd "$(pwdx $customPPID | cut -d' ' -f2)"
		git_status_lines=$(git status -s 2>&1)
		if [ "$(echo "$git_status_lines" | grep -oi "fatal")" = "fatal" ] ; then
			echo "rgb 00000000" >$ckb_pipe
			sleep 5
		else
			echo "$git_status_lines" | while read line ; do
				git_symbol=${line:0:2}
				if [ $git_symbol ] ; then
					if [ "$git_symbol" = "??" ] ; then # this case happens when there's an untracked file
						echo "rgb logo:ff0000ff" >$ckb_pipe #set the color to red
						sleep $timer
					else # this case happens when a file is added/modified/deleted/moved
						echo "rgb logo:ff8c00ff" >$ckb_pipe #set the color to darkorange
						sleep $timer
					fi
					echo "rgb logo:000000ff" >$ckb_pipe
					sleep $timer
				else #this case happens when there's no unstaged change in the git repository
					echo "rgb logo:00ff00ff" >$ckb_pipe #set the color to green
					sleep $timer
				fi
			done
		fi
	done
fi

#reset the color after some time, there's a small bug without that
sleep $timer
echo "rgb 00000000" >$ckb_pipe
